var applicationContext = require("./application-context");

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require("mongoose").set('debug',true);

var dbConnection = mongoose.connection;
// default db setting
dbConnection.on("error", function(err) {
  console.log("Mongo Db Error: " + err.toString());
});
dbConnection.on("connected", function(){
  console.log("Mongo Db Connected: " + applicationContext.getDbConnectionUri());
});

var db;
db = mongoose.connect(applicationContext.getDbConnectionUri(), applicationContext.getDbConnectionSettings());

var index = require('./routes/index');
var users = require('./routes/users');
var policies = require('./routes/policies');
var counties = require('./routes/counties');
var resources = require("./routes/resources");
var searchKeywords = require("./routes/searchKeywords");

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/', resources);
app.use('/users', users);
app.use('/api/policies', policies);
app.use('/api/counties', counties);
app.use("/api/searchKeywords", searchKeywords);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
