"use strict";

// Applicationwise variables
var applicationDirectory = __dirname;
var dbConnectionUri = "mongodb://localhost/PolicyFinder";
var dbAuthenticationName = "";  // placeholder
var dbAuthenticationPassword = "";  // placeholder
var localContentCacheDirectory = "";

var serverPort = 3030;

var dbConnectionSettings = {
  socketTimeoutMS: 0,
  keepAlive: true,
  reconnectTries: 30
};

// Application Context
var ApplicationContext = {
  getApplicationDirectory: function () {
    return applicationDirectory;
  },
  getLocalContentCacheDirectory: function () {
    return localContentCacheDirectory;
  },
  getDbConnectionUri: function () {
    return dbConnectionUri;
  },
  getServerPort: function () {
    return serverPort;
  },
  getDbConnectionSettings: function () {
    return dbConnectionSettings;
  }
};

module.exports = ApplicationContext;