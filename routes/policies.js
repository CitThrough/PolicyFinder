var express = require('express');
var router = express.Router();
var PolicyModel = require("../models/policyModel");

function addArticleNameFilter(articleNameRequest, query) {
  if (articleNameRequest !== "" && articleNameRequest !== undefined) {
    var articleNameFragments = articleNameRequest.split(" ");
    var articleNameTempArray = [];
    articleNameFragments.forEach(function (articleNameFragment, index) {
      articleNameTempArray.push("(" + articleNameFragment + "*)");
    });
    articleNameFragments = articleNameTempArray.join("\\s");
    var articleNamePattern = new RegExp(articleNameFragments, "ig");
    query.articleName = articleNamePattern;
  }
}

function addCountyFilter(countyRequest, query) {
  if (countyRequest !== undefined && countyRequest.length > 0) {
    query.county = {
      $in: countyRequest
    };
  }
}

function addArticleDateFilter(articleDateRequest, query) {
  // TODO: loosen the logic
  if (articleDateRequest !== undefined) {
    var dateRange = articleDateRequest.split(",");
    if (dateRange != undefined && dateRange.length == 2) {
      query.articleDate = {
        $gte: dateRange[0],
        $lte: dateRange[1]
      };
    }
  }
}

function addSearchKeywordsFilter(searchKeywordsRequest, query) {
  if (searchKeywordsRequest !== undefined && searchKeywordsRequest.length > 0) {
    query.searchKeywords = {
      $in: searchKeywordsRequest
    };
  }
}

/* GET users listing. */
router.route("/")
  .get(function (req, res, next) {
    var query = {};
    addArticleNameFilter(req.query.articleName, query);
    addCountyFilter(req.query.county, query);
    addArticleDateFilter(req.query.articleDate, query);
    addSearchKeywordsFilter(req.query.searchKeywords, query);
    PolicyModel.find(query, function (err, policies) {
      if (err) {
        res.status(500).send(err.message);
      }
      else {
        var returnPolicies = [];
        policies.forEach(function (policy, index) {
          var newPolicy = policy.toJSON();
          newPolicy.link = {};
          newPolicy.link.self = "http://" + req.headers.host + "/api/policies/" + newPolicy._id;
          returnPolicies.push(newPolicy);
        });
        res.json(returnPolicies);
      }
    });
  })
  .post(function (req, res) {
    if (req.body._id == "") {
      delete req.body._id;
      var policyEntry = new PolicyModel(req.body);
      // TODO: request validation
      policyEntry.save(function (err, policyEntry) {
        if (err) {
          res.status(500).send(err.message);  // direct back to index after creation for now
        }
        else {
          console.log(JSON.stringify((policyEntry)));
          res.status(200).send(policyEntry);  // direct back to index after creation for now
        }
      });
    } else {
      PolicyModel.findById(req.body._id, function (err, policy) {
        if (err) {
          res.status(500).send(err.message);
        }
        else {
          policy.articleName = req.body.articleName;
          policy.documentLink = req.body.documentLink;
          policy.county = req.body.county;
          policy.articleDate = req.body.articleDate;
          policy.searchKeywords = req.body.searchKeywords;
          policy.articleSummary = req.body.articleSummary;
          policy.save(function (err) {
            if (err) {
              res.status(500).send(err.message);  // direct back to index after creation for now
            }
            else {
              console.log(JSON.stringify((policyEntry)));
              res.status(200).send(policy);  // direct back to index after creation for now
            }
          });
        }
      })
    }
  });

router.use("/:policyId", function (req, res, next) {
  PolicyModel.findById(req.params.policyId, function (err, policy) {
    if (err) {
      res.status(500).send(err.message);
    }
    else if (policy) {
      req.policy = policy;
      next();
    }
    else {
      res.status(404).send("no policy found");
    }
  })
});
router.route("/:policyId")
  .get(function (req, res) {
    var returnPolicy = req.policy.toJSON();
    res.json(returnPolicy);
  })
  .put(function (req, res) {
    // TODO: validate input
    req.policy = req.body;
    req.policy.save(function (err) {
      if (err) {
        res.status(500).send(err.message);
      }
      else {
        res.json(req.policy);
      }
    });
  })
  .patch(function (req, res) {
    if (req.policy._id)
      delete req.policy._id;

    for (var p in req.body) {
      req.policy[p] = req.body[p];
    }

    req.policy.save(function (err) {
      if (err)
        res.status(500).send(err);
      else {
        res.json(req.policy);
      }
    });
  })
  .delete(function (req, res) {
    req.policy.remove(function (err) {
      if (err)
        res.status(500).send(err.message);
      else {
        res.status(204).send('Removed');
      }
    });
  });

module.exports = router;
