var express = require('express');
var router = express.Router();
var CountyModel = require("../models/countyModel");
var applicationContext = require("../application-context");

router.route("/images/:imageName")
  .get(function(req, res) {
    res.sendfile(applicationContext.getApplicationDirectory() + "public/images/" + req.params.imageName);
  });
module.exports = router;