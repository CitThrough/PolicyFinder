var express = require('express');
var router = express.Router();
var PolicyModel = require("../models/policyModel");

router.route("/")
  .get(function (req, res, next) {
    query = {};
    columnSelection = {searchKeywords: 1};
    PolicyModel.distinct("searchKeywords", function (err, searchKeywords) {
      if (err) {
        res.status(500).send(err.message);
      }
      else {
        var searchKeywordsJson = {
          "searchKeywords" : searchKeywords
        };
        res.send(searchKeywords);
      }
    });
  });

module.exports = router;