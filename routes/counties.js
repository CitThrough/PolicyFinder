var express = require('express');
var router = express.Router();
var CountyModel = require("../models/countyModel");

router.route("/")
  .get(function(req, res, next) {
    var query = {};
    CountyModel.find(query, function(err, counties) {
      if (err) {
        res.status(500).send(err.message);
      }
      else {
        var returnCounties = [];
        counties.forEach(function(county, index, array) {
          var newCounty = county.toJSON();
          returnCounties.push(newCounty);
        });
        res.json(returnCounties);
      }
    });
  });
module.exports = router;