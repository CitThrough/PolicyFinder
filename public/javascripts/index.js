$(document).ready(function () {
  // date picker
  $("#articleDate").datepicker({
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years"
  });
  // date range picker
  $("#yearRangeSelection").slider({
    id: "yearRangeSlider"
  });
  $("#yearRangeSlider").addClass("form-control");

  // update county list
  // TODO: change the query to jquery for consistency
  var retrieveCountyListRequest = new XMLHttpRequest();
  retrieveCountyListRequest.open("Get", "/api/counties", false);
  retrieveCountyListRequest.send(null);
  var countyList = [];
  if (retrieveCountyListRequest.status == 200) {
    countyList = JSON.parse(retrieveCountyListRequest.responseText);
  }
  $.each(countyList, function (i, county) {
    $("#county").append($("<option>", {
      "data-tokens": county.countyName
    }).text(county.countyName));
    $("#countySelection").append($("<option>", {
      "data-tokens": county.countyName
    }).text(county.countyName));
  });
  $("#county").selectpicker();  // update again after getting county list
  $("#countySelection").selectpicker();  // update again after getting county list

  // update search keywords

  // TODO: change the query to jquery for consistency
  var retrievekeywordsListRequest = new XMLHttpRequest();
  retrievekeywordsListRequest.open("Get", "/api/searchKeywords", false);
  retrievekeywordsListRequest.send(null);
  var searchKeywords = [];
  if (retrievekeywordsListRequest.status == 200) {
    searchKeywords = retrievekeywordsListRequest.responseText;
  }
  $.each(JSON.parse(searchKeywords), function (i, searchKeyword) {
    $("#keywordsSelection").append($("<option>", {
      "data-tokens": searchKeyword
    }).text(searchKeyword));
  });
  $("#keywordsSelection").selectpicker();  // update again after getting county list


  $("#policySearchForm").submit(function (event) {
    event.preventDefault();
    $("#resultContainer").empty();  // clear previous result
    $.ajax({
      data: $(this).serialize(),
      type: $(this).attr("method"),
      url: $(this).attr("action"),
      success: function (policyList) {
        if (policyList.length > 0) {
          createResultPlaceHolder("Results Matching Your Search");
          $.each(policyList, function (i, policy) {
            var $resultPlaceHolderTag = $("#resultPlaceHolder");
            addPolicyDisplay($resultPlaceHolderTag, policy);
          });
        } else {
          createResultPlaceHolder("No Entry found.");
        }
        $.notify(policyList.length + " result(s) found.", {
          className: "info",
          globalPosition: "top center"
        });
        $(window).scrollTop($('#resultContainer').offset().top);
      }
    });
  });
  $("#policyCreationForm").submit(function (event) {
    event.preventDefault();
    $.ajax({
      data: $(this).serialize(),
      type: $(this).attr("method"),
      url: $(this).attr("action"),
      success: function (policy) {
        $.notify(policy.articleName + " has been successfully saved.", {
          className: "success",
          globalPosition: "top center"
        });
        var policyContentId = "#" + policy._id + "-content";
        if ($(policyContentId).length) {  // check if the entry we modified is in the result set
          updatePolicyContent(policy);
          updatePolicyViewEditControl(policy);
        }
      },
      error: function(xhr, err) {
        $.notify(policy.articleName + " failed to save", {
          className: "error",
          globalPosition: "top center"
        })
      }
    });
    $("#myModal").modal("hide");
  });

  // Set up advance drop down
  $("#advanceSearchButton").on("click", function (event) {
    $(this).parent().toggleClass("open");
  });
  $("body").on("click", function(event) {
    var $advanceSearchOption = $(event.target).closest("#advanceSearchOptions"); // this should either return length of 0 or 1
    if ($advanceSearchOption.length === 0) {
      $("#advanceSearchOptions").removeClass("open"); // remove the tag if we are not under the #advanceSearchOptions tag
    }
  });

  $("#openPolicyModal").on("click", function (event){
    resetPolicyForm();
    // HACK changing the modal button text
    $("#policyCreateOrUpdateButton").val("Create");
    $("#myModal").modal("show");
  });

  function createResultPlaceHolder(message) {
    $("#resultContainer").append($("<h2>", {
      id: "resultHeader"
    }).text(message)).append($("<div>", {
      class: "row"
    }).append($("<div>", {
      id: "resultPlaceHolder",
      class: "list-group"
    })));
  }

  function addPolicyImage($rootElement, policy) {
    var $mediaTag = $("<div>", {
      class: "media col-md-3"
    });
    var $figureTag = $("<figure>", {
      class: "pull-left"
    });
    var $imageTag = $("<img>", {
      class: "media-object img-rounded img-responsive",
      src: "/images/nyic-logo.png",
      alt: "placehold.it/350x250"
    });
    $figureTag.append($imageTag);
    $mediaTag.append($figureTag);
    $rootElement.append($mediaTag);
  }

  function addPolicyViewEditControl($rootElement, policy) {
    var $buttonDivTag = $("<div>", {
      class: "btn-group-vertical col-md-2"
    })
      .append($("<button>", {
        class: "btn btn-md btn-primary",
        id: policy._id + "-edit"
      }).append($("<span>", {
        class: "glyphicon glyphicon-wrench"
      })).append(" Edit").on("click", function() {
        updatePolicyForm(policy);
        // HACK changing the modal button text
        $("#policyCreateOrUpdateButton").val("Update");
        $("#myModal").modal("show");
      }))

      .append($("<button>", {
        class: "btn btn-md btn-info",
        id: policy._id + "-view"
      }).append($("<span>", {
        class: "glyphicon glyphicon-search"
      })).append(" View").on("click", function() {
        window.open(policy.documentLink);
      }))

      .append($("<button>", {
        class: "btn btn-md btn-danger"
      }).append($("<span>", {
        class: "glyphicon glyphicon-trash"
      })).append(" Delete").on("click", function() {
        var $policyRemoveContentTag = $("#policyRemoveContent");
        $policyRemoveContentTag.empty();
        var $policyTitleTag = $("<h3>", {
        }).text(policy.articleName);
        var $policySummaryTag = $("<p>", {
        }).text(policy.articleSummary);
        $policyRemoveContentTag.append($policyTitleTag).append($policySummaryTag);
        $("#policyConfirmDeleteButton").off("click").on("click", function() {
          $.ajax({
            data: policy,
            type: "delete",
            url: "/api/policies/" + policy._id,
            success: function () {
              $.notify(policy.articleName + " has been successfully removed.", {
                className: "success",
                globalPosition: "top center"
              });
              var policyContainerId = "#" + policy._id + "-container";
              $(policyContainerId).remove();
              $("#myRemoveModal").modal("hide");
            }
          })
        });
        $("#myRemoveModal").modal("show");
      }));

    $rootElement.append($buttonDivTag);
  }

  function addPolicyContent($rootElement, policy) {
    var $contentTag = $("<div>", {
      class: "col-md-10",
      id: policy._id  + "-content"   // use to identify the correct policy content
    });
    var $policyTitleTag = $("<h3>", {
      class: "list-group-item-heading"
    }).text(policy.articleName);
    var $policySummaryTag = $("<p>", {
      class: "list-group-item-text"
    }).text(policy.articleSummary);
    var $hrTag = $("<hr>");
    $contentTag.append($policyTitleTag).append($policySummaryTag).append($hrTag);
    $rootElement.append($contentTag);
  }

  function updatePolicyContent(policy) {
    var policyContentId = "#" + policy._id + "-content";
    $(policyContentId + " > h3").text(policy.articleName);
    $(policyContentId + " > p").text(policy.articleSummary);
  }
  function updatePolicyViewEditControl(policy) {
    var policyEditButtonId = "#" + policy._id + "-edit";
    $(policyEditButtonId).off("click").on("click", function() {
      updatePolicyForm(policy);
      // HACK changing the modal button text
      $("#policyCreateOrUpdateButton").val("Update");
      $("#myModal").modal("show");
    });
    var policyViewButtonId = "#" + policy._id + "-view";
    $(policyViewButtonId).off("click").on("click", function() {
      window.open(policy.documentLink);
    });
  }

  function resetPolicyForm() {
    var policy = {};
    policy.county = "";
    policy.articleDate = "";
    policy.articleName = "";
    policy.documentLink="";
    policy.articleSummary = "";
    policy._id = "";
    updatePolicyForm(policy);
  }
  function addPolicyDisplay($rootElement, policy) {
    var $anchorTag = $("<a>", {
      class: "list-group-item",
      style: "border: none",
      id : policy._id + "-container"
    });
    //addPolicyImage($anchorTag, policy);
    addPolicyViewEditControl($anchorTag, policy);
    addPolicyContent($anchorTag, policy);
    $rootElement.append($anchorTag);
  }

  function updatePolicyForm( policy) {
    $("#county").val(policy.county);
    $("#articleDate").val(policy.articleDate);
    $("#articleName").val(policy.articleName);
    $("#searchKeywords").tagsinput("removeAll");
    if (policy.searchKeywords != undefined && policy.searchKeywords.constructor === Array) {
      policy.searchKeywords.forEach(function (searchKeyword, index) {
        $("#searchKeywords").tagsinput("add", searchKeyword);
      })
    }

    $("#searchKeywords").tagsinput("refresh");
    $("#documentLink").val(policy.documentLink);
    $("#articleSummary").val( policy.articleSummary);
    $("#articleId").val(policy._id);
  }
});