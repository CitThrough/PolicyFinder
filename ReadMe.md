# Installation Guide (Windows)
## Install Node
1. [Download](https://nodejs.org/en/download/) NodeJs .msi file.
2. [Install](http://blog.teamtreehouse.com/install-node-js-npm-windows) NodeJs.

## Install MongoDb
1. [Download](https://www.mongodb.com/download-center?#community) MongoDb Community Version.
2. [Install](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/) MongoDb.

## Install Git
1. [Download](https://git-scm.com/downloads) and [Install](https://www.atlassian.com/git/tutorials/install-git#windows) Git

## Downlaod and Install PolicyFinder Project
1. Clone the project repository with the git command in the command prompt. i.e. (C:\\) This would create a PolicyFinder directory. (C:\\PolicyFinder)

        git clone https://gitlab.com/CitThrough/PolicyFinder.git

2. cd into the PolicyFinder direcotry
 
        cd PolicyFinder

3. Check out policyfinder dependency using node command

        npm install

## Setup and Start MongoDb 
1. Create the following directory structure for MongoDb data storage and logging

            mkdir c:\data\db
            mkdir c:\data\log

2. Create a file at C:\Program Files\MongoDB\Server\3.6\mongod.cfg that specifies both systemLog.path and storage.dbPath:

            systemLog:
                destination: "file"
                path: "c:\\data\\log\\mongod.log"
            storage:
                dbPath: "c:\\data\\db"

3. Install the MongoDB service by starting mongod.exe with the --install option and the -config option to specify the previously created configuration file. (This should enable mongoDb to run on start up)

        "C:\Program Files\MongoDB\Server\3.6\bin\mongod.exe" ^
        --config "C:\Program Files\MongoDB\Server\3.6\mongod.cfg" ^
        --install

4. Make sure that the service is running by checking `services.msc` in windows.

5. Import county list into mongoDb with the following command

            mongoimport --db PolicyFinder --collection counties --type csv --headerline --file C:\Git\PolicyFinderNew\CountyList.txt


## Run Node Project
1. Go into the project directory


           C:\PolicyFinder\bin\
    
2. Execute www.js file


          node www
     
3. Server should be running on port 3030. Connect to the server in the brower with the following url


        http://127.0.0.1:3030/


# Policy Findr Common Commands
1. Export mongo db database

            mongoexport --db PolicyFinder --collection policies --out policies.json

2. Import mongo db database

            mongoimport --db PolicyFinder --collection policies --file policies.json