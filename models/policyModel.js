var mongoose = require("mongoose");
var Schema = mongoose.Schema;

// NOTE: add local document directory
var policySchema = new Schema({
  articleName: String,
  documentLink: {type: String, required: true},
  county: String,
  articleDate: Number,
  searchKeywords: [{type: String}],  // array string
  articleSummary: String
});

module.exports = mongoose.model("Policy", policySchema, "policies");
