var mongoose = require("mongoose");
var Schema = mongoose.Schema;

// NOTE: add local document directory
var countySchema = new Schema({
  countyName: String
});

module.exports = mongoose.model("County", countySchema, "counties");
